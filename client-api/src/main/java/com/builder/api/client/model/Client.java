package com.builder.api.client.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="CLIENT")
public class Client extends SuperEntity{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8458803559776026889L;
	@Column(name="nome",length=180,nullable=true)
	private String name;
	@Column(name="cpf",length=15,nullable=true)
	private String cpf;
	@Column(name="dataNascimento")
	@Temporal(TemporalType.DATE)
	private Date bith;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public Date getBith() {
		return bith;
	}
	public void setBith(Date bith) {
		this.bith = bith;
	}
	
	

}
