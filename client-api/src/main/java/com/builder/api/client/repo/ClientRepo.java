package com.builder.api.client.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.builder.api.client.model.Client;

public interface ClientRepo extends JpaRepository<Client, Long> {
	
	@Query(value = "SELECT c FROM Client c WHERE c.cpf = :cpf ORDER BY id")
	Page<Client> findAllCPF(Pageable pageable, @Param("cpf") String cpf);

	@Query(value = "SELECT c FROM Client c WHERE lower(c.name) like lower(concat('%', :name,'%')) ORDER BY id")
	Page<Client> findAllByName(Pageable pageable, @Param("name") String name);


}
