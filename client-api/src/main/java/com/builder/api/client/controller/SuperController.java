package com.builder.api.client.controller;

import org.springframework.http.ResponseEntity;

public class SuperController {
	
	protected static <T> ResponseEntity<T> responseOK(T body) {
		return ResponseEntity.ok(body);
	}

}
