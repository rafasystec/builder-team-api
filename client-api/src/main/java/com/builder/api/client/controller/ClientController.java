package com.builder.api.client.controller;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.builder.api.client.model.Client;
import com.builder.api.client.repo.ClientRepo;
import com.builder.api.client.response.ResponseClient;

@RestController
@RequestMapping("/client")
public class ClientController extends SuperController{
	
	
	@Autowired
	ClientRepo clientRepo;
	
	@GetMapping("on")
	public ResponseEntity<String> getOn() {
		System.out.println("Save successful!");
		return ResponseEntity.ok("client on");
	}
		
	@GetMapping("{id}")
	@Transactional
	public ResponseEntity<Client> get(@PathVariable("id") long id) {
		Optional<Client> optional = clientRepo.findById(id);
		if(optional.isPresent()) {
			return ResponseEntity.ok(optional.get());
		}else {
			return ResponseEntity.notFound().build();
		}
		
	}
	
	@PostMapping
	@Transactional
	public ResponseEntity<Client> save(@RequestBody Client client) {
		client = clientRepo.save(client);
		System.out.println("Save successful!");
		return ResponseEntity.ok(client);
	}
	
	@PutMapping
	@Transactional
	public ResponseEntity<Client> update(@RequestBody Client client) {
		if(client.getId() > 0) {
			Optional<Client> findById = clientRepo.findById(client.getId());
			if(findById.isPresent()) {
				clientRepo.save(client);
			}else {
				ResponseEntity.badRequest().body("No client with that ID is present!");
			}
		}else {
			ResponseEntity.badRequest().body("No client found!");
		}
		System.out.println("Update successful!");
		return ResponseEntity.ok(client);
	}
	
	@DeleteMapping("{id}")
	@Transactional
	public ResponseEntity<String> delete(@PathVariable("id") long id) {
		clientRepo.deleteById(id);
		System.out.println("Deleted successful!");
		return ResponseEntity.ok("deleted");
	}
	
	@GetMapping("by/cpf/{page}/{pageSize}/{cpf}")
	public ResponseEntity<List<ResponseClient>> getByCPF(@PathVariable int page, @PathVariable int pageSize,
			@PathVariable String cpf) {
		Pageable pagination = PageRequest.of(page, pageSize);
		/*
		 * -------------------------------------------------------------------------------------
		 * NOTE: Return the Page could be not so good, so you can get only specific data from it.
		 * ------------------------------------------------------------------------------------- 
		 */
		Page<Client> list = clientRepo.findAllCPF(pagination,cpf);
		return ResponseEntity.ok(ResponseClient.toList(list));
	}
	
	@GetMapping("by/name/{page}/{pageSize}/{name}")
	public ResponseEntity<List<ResponseClient>> getByName(@PathVariable int page, @PathVariable int pageSize,
			@PathVariable String name) {
		Pageable pagination = PageRequest.of(page, pageSize);
		/*
		 * -------------------------------------------------------------------------------------
		 * NOTE: Return the Page could be not so good, so you can get only specific data from it.
		 * ------------------------------------------------------------------------------------- 
		 */
		Page<Client> list = clientRepo.findAllByName(pagination,name);
		return ResponseEntity.ok(ResponseClient.toList(list));
	}

}
