package com.builder.api.client.util;

import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtil {
	
	public static int getAge(LocalDate birthDate) {
		if (birthDate != null) {
			return Period.between(birthDate, LocalDate.now()).getYears();
		} else {
			return 0;
		}
	}
	
	public static int getAge(Date birthDate) {
		return calculateAge(birthDate);
	}
	
	public static int calculateAge(java.util.Date dataNasc){
		Calendar dateOfBirth = new GregorianCalendar();
		dateOfBirth.setTime(dataNasc);
		Calendar today = Calendar.getInstance();
		int age = today.get(Calendar.YEAR) - dateOfBirth.get(Calendar.YEAR);
		dateOfBirth.add(Calendar.YEAR, age);
		if (today.before(dateOfBirth)) {
			age--;
		}
		return age;
	}

}
