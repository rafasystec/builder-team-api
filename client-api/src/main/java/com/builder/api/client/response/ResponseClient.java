package com.builder.api.client.response;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;

import com.builder.api.client.model.Client;
import com.builder.api.client.util.DateUtil;

import br.com.caelum.stella.format.CPFFormatter;

public class ResponseClient {
	
	private String name;
	private String cpf;
	private int age;

	public static List<ResponseClient> toList(Page<Client> list) {
		CPFFormatter formatter = new CPFFormatter();
		List<ResponseClient> gList = new ArrayList<ResponseClient>();
		if(list != null && list.getSize() > 0) {
			list.forEach(client ->{
				ResponseClient c = new ResponseClient();
				c.setName(client.getName());
				String cpf = client.getCpf();
				if(!formatter.isFormatted(cpf)) {
					c.setCpf(formatter.format(cpf));
				}else {
					c.setCpf(cpf);
				}
				c.setAge(DateUtil.getAge(client.getBith()));
				gList.add(c);
			});
		}
		return gList;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

}
