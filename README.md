**Postman file to import

inside the builder-team-api/client-api/src/main/resources folder there is a json file to import with the name: client-api.postman_collection.json. Import this file by Postman
---

The application was deployed on Heroku at this link below:

https://builder-client-api.herokuapp.com

To Test the application we registered onde person, just make a GET to https://builder-client-api.herokuapp.com/client/1 on your browser to see it working.

Database is Postgres

**POST Request
---

URL: https://builder-client-api.herokuapp.com/client
body:

{
    "name" : "Luiza Sousa Mello",
    "cpf"  : "98547856419",
    "bith" : "1971-04-21"
    
}

**GET by CPF 
---

URL: https://builder-client-api.herokuapp.com/client/by/cpf/{page}/{total-result_per-page}/{cpf}
Where: 
1. {page} is the number of the page starting at zero
2. {total-result_per-page} is the size of the page
3. {cpf} CPF info to query

Ex: https://builder-client-api.herokuapp.com/client/by/cpf/0/3/65841214785

**GET by name
---

URL: https://builder-client-api.herokuapp.com/client/by/name/{page}/{total-result_per-page}/{name}
Where: 
1. {page} is the number of the page starting at zero
2. {total-result_per-page} is the size of the page
3. {name} Name info to query

Ex: https://builder-client-api.herokuapp.com/client/by/name/0/3/rafael

**DELETE
---

To delete a resouce use
URL:https://builder-client-api.herokuapp.com/client/{id}
Where: {id} is the client id to delete

Ex: https://builder-client-api.herokuapp.com/client/1

**PUT
---

To change a resource send a JSON client with its id
URL:https://builder-client-api.herokuapp.com/client

body: 

{
    "id": 1,
    "register": "2020-10-15T07:21:48.878+00:00",
    "name": "Antonio Rafael",
    "cpf": "11111111111",
    "bith": "1982-04-14"
}